package main

import (
	"log"
	"net/http"
)

const (
	Host = ""
	Port = "8000"
)

func main() {
	fileServer := http.FileServer(http.Dir("./dist"))
	http.Handle("/", fileServer)
	err := http.ListenAndServe(Host+":"+Port, nil)
	if err != nil {
		log.Fatal("Error Starting the HTTP Server :", err)
		return
	}
}
